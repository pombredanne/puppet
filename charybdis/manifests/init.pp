# Install Charybdis IRC server.
#
class charybdis(
  $sid='00A',
  $server_name='irc.localdomain',
  $server_description='IRC Server',
  $network_name='IRC',
  $network_description='IRC Network',
  $admin_name='Administrator',
  $admin_description='IRC Administrator',
  $admin_email='root@localhost',
  $motd=undef,
  $motd_source=undef,
  $port='6667',
  $sslport='6668',
  $ssl_dh=undef,
  $ssl_key=undef,
  $ssl_cert=undef,
  $users=['*@*'],
  $operators=[],
  $umodes=undef,
  $cloaking=true,
  $hub=false,
  $ident=false,
  $throttle_count='4',
  $throttle_duration='60',
  $services=false,
  $services_name='ircservices.localdomain',
  $services_password=undef,
) {

  case $::operatingsystem {
    'ubuntu': { }
    default: {
      fail("charybdis not supported on ${::operatingsystem}")
    }
  }

  if $services == true and ! $services_password {
    fail('Must define $services_password')
  }

  if $umodes {
    $umodes_real = $umodes
  } else {
    if $cloaking == true {
      $umodes_real = '+ix'
    } else {
      $umodes_real = '+i'
    }
  }

  package { 'charybdis':
    ensure => installed,
  }

  file { '/etc/charybdis/ircd.conf':
    ensure  => present,
    mode    => '0640',
    owner   => 'root',
    group   => 'charybdis',
    content => template('charybdis/ircd.conf.erb'),
    require => Package['charybdis'],
    notify  => Service['charybdis'],
  }

  file { '/etc/charybdis/ircd.motd':
    ensure  => present,
    mode    => '0640',
    owner   => 'root',
    group   => 'charybdis',
    content => $motd,
    source  => $motd_source,
    require => Package['charybdis'],
    notify  => Service['charybdis'],
  }

  if $ssl_key and $ssl_cert {
    file { '/etc/charybdis/ircd.key':
      ensure  => present,
      mode    => '0640',
      owner   => 'root',
      group   => 'charybdis',
      source  => $ssl_key,
      require => Package['charybdis'],
      notify  => Service['charybdis'],
    }

    file { '/etc/charybdis/ircd.crt':
      ensure  => present,
      mode    => '0640',
      owner   => 'root',
      group   => 'charybdis',
      source  => $ssl_cert,
      require => Package['charybdis'],
      notify  => Service['charybdis'],
    }

    if $ssl_dh {
      file { '/etc/charybdis/dh.pem':
        ensure  => present,
        mode    => '0640',
        owner   => 'root',
        group   => 'charybdis',
        source  => $ssl_dh,
        require => Package['charybdis'],
        notify  => Service['charybdis'],
      }
    } else {
      ssl::dhparam { '/etc/charybdis/dh.pem':
        mode    => '0640',
        owner   => 'root',
        group   => 'charybdis',
        require => Package['charybdis'],
        notify  => Service['charybdis'],
      }
    }
  }

  service { 'charybdis':
    ensure => running,
    enable => true,
  }

  file { '/var/log/charybdis':
    ensure  => directory,
    mode    => '0640',
    owner   => 'charybdis',
    group   => 'charybdis',
    recurse => true,
    require => Service['charybdis'],
  }

}
