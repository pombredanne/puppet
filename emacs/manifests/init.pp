
# Install Emacs
#
class emacs {

    package { "emacs":
        ensure => installed,
    }

    if $::puppetversion =~ /^2\./ {
        Package["emacs"] {
            flavor => "no_x11",
        }
    }

}
