# Install abusesa-analytics.
#
class abusesa::analytics {

  if ! $abusesa_analytics_package {
    if $::abusesa_analytics_package_latest {
      $abusesa_analytics_package = $::abusesa_analytics_package_latest
    } else {
      fail('Must define $abusesa_analytics_package or $abusesa_analytics_package_latest')
    }
  }

  python::pip::install { 'abusesa-analytics.tar.gz':
    source => "puppet:///files/packages/${abusesa_analytics_package}",
  }

  Python::Pip::Install['abusesa.tar.gz'] ->
  Python::Pip::Install['abusesa-analytics.tar.gz']

}
