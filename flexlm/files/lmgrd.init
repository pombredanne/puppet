#!/bin/bash
#
# lmgrd     Starts lmgrd license daemon.
#
# chkconfig: - 99 01
# description: Flexnet license manager daemon.

# Source function library.
. /etc/init.d/functions

PATH=/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin:/usr/local/lib/lmgrd

# Determine license id
licid=`basename $0 | cut -d '.' -f 2-`
if [ -z ${licid} ]; then
    licid=`hostname -s`
fi

logdir="/var/log/lmgrd/${licid}"
logfile="${logdir}/lmgrd.${licid}.`date '+%Y%m%d-%H%M%S'`.log"
licfile="/etc/lmgrd/license.${licid}"
lockfile="/var/lock/lmgrd.${licid}"

start() {
    touch ${logfile}
    chown licensed:root ${logfile}
    chmod 640 ${logfile}
    echo -n $"Starting lmgrd (${licid}): "
    su - licensed -s /bin/sh -c "lmgrd -l ${logfile} -x lmdown -2 -p -c ${licfile}"
    RETVAL=$?
    if [ ${RETVAL} -eq 0 ]; then
        lmstat -c ${licfile} > /dev/null 2>&1
        RETVAL=$?
    fi
    if [ ${RETVAL} -eq 0 ]; then
        echo_success
        touch ${lockfile}
    else
        echo_failure
    fi
    echo
    return ${RETVAL}
}

stop() {
    echo -n $"Shutting down lmgrd (${licid}): "
    pkill -u licensed -f "${logdir}/lmgrd.${licid}"
    RETVAL=$?
    if [ $RETVAL -eq 0 ]; then
        echo_success
        rm -f $lockfile
    else
        echo_failure
    fi
    echo
    return ${RETVAL}
}

case "$1" in
  start)
        start
        ;;
  stop)
        stop
        ;;
  status)
        lmstat -c `lmstat -c ${licfile} | \
            sed -n 's/License server status: \([0-9]*@[^ ]*\)/\1/p'`
        ;;
  restart)
        stop
        start
        ;;
  reload)
        lmreread -c ${licfile}
        ;;
  *)
        echo $"Usage: $0 {start|stop|restart|condrestart|reload|status}"
        exit 2
        ;;
esac

exit $?
