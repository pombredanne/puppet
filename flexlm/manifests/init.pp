
# Install FlexLM license tools
#
class flexlm::client {

    if $::kernel == "Linux" {
        require lsb::core
    }

    file { "/usr/local/bin/lmutil":
        ensure => present,
        source => "puppet:///files/packages/lmutil-${lmutil_package_latest}.${::architecture}.${::kernel}",
        mode   => "0755",
        owner  => "root",
        group  => "root",
    }

    file { [
      "/usr/local/bin/lmborrow",
      "/usr/local/bin/lmcksum",
      "/usr/local/bin/lmdiag",
      "/usr/local/bin/lmdown",
      "/usr/local/bin/lmhostid",
      "/usr/local/bin/lminstall",
      "/usr/local/bin/lmnewlog",
      "/usr/local/bin/lmpath",
      "/usr/local/bin/lmremove",
      "/usr/local/bin/lmreread",
      "/usr/local/bin/lmstat",
      "/usr/local/bin/lmswitch",
      "/usr/local/bin/lmver",
    ]:
        ensure  => link,
        target  => "lmutil",
        owner   => "root",
        group   => "root",
        require => File["/usr/local/bin/lmutil"],
    }

}


# Install common files from FlexLM license server
#
class flexlm::lmgrd::common {

    require flexlm::client

    include user::system
    realize(User["licensed"], Group["licensed"])

    file { "/usr/local/sbin/lmgrd":
        ensure => present,
        source => "puppet:///files/packages/lmgrd-${lmgrd_package_latest}.${::architecture}.${::kernel}",
        mode   => "0755",
        owner  => "root",
        group  => "root",
    }

    file { [ "/etc/lmgrd", "/usr/local/lib/lmgrd", "/var/log/lmgrd", ]:
        ensure => directory,
        mode   => "0755",
        owner  => "root",
        group  => "root",
    }

}


# Install new instance of lmgrd
#
# === Parameters
#
#     $name:
#         Instance name.
#     $license:
#         Source path for license file.
#     $vendors:
#         Array containing vendor daemon names to be installed. They are
#         installed under /usr/local/lib/lmgrd from:
#            puppet:///files/lmgrd/$name
#
# === Sample usage
#
# flexlm::lmgrd { "matlab":
#     license => "puppet:///files/lmgrd/license.matlab",
#     vendors => [ "lm_matlab", ],
# }
#
define flexlm::lmgrd($license, $vendors=[]) {

    require flexlm::lmgrd::common

    if ! ($::operatingsystem in ["CentOS","RedHat"]) {
        fail("flexlm::lmgrd not supported in ${::operatingsystem}")
    }

    file { "/etc/lmgrd/license.${name}":
        ensure => present,
        source => $license,
        mode   => "0644",
        owner  => "root",
        group  => "root",
        notify => Service["lmgrd.${name}"],
    }

    file { "/var/log/lmgrd/${name}":
        ensure => directory,
        mode   => "0750",
        owner  => "root",
        group  => "licensed",
        before => Service["lmgrd.${name}"],
    }

    flexlm::lmgrd::vendor { $vendors: }

    file { "/etc/init.d/lmgrd.${name}":
        ensure  => present,
        source  => "puppet:///modules/flexlm/lmgrd.init",
        mode    => "0755",
        owner   => "root",
        group   => "root",
        notify  => Exec["chkconfig --add lmgrd.${name}"],
    }
    exec { "chkconfig --add lmgrd.${name}":
        user        => "root",
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        refreshonly => true,
        before      => Service["lmgrd.${name}"],
    }
    service { "lmgrd.${name}":
        ensure    => running,
        enable    => true,
        hasstatus => true,
    }

}


# Install vendor daemon binary
#
# === Parameters:
#
#     $name:
#         Vendor daemon name.
#     $source:
#         Source path where daemon is found. Defaults to
#         puppet:///files/lmgrd/${name}.
#
# === Sample usage:
#
# flexlm::lmgrd::vendor { "lm_matlab":
#     source => "puppet:///files/lmgrd/lm_matlab",
# }
#
define flexlm::lmgrd::vendor($source=undef) {

    if !$source {
        $source_real = "puppet:///files/lmgrd/${name}"
    } else {
        $source_real = $source
    }

    file { "/usr/local/lib/lmgrd/${name}":
        ensure  => present,
        source  => $source_real,
        mode    => "0755",
        owner   => "root",
        group   => "root",
        require => File["/usr/local/lib/lmgrd"],
    }

}

