# Install time (time-stream) server.
#
class time::server {

    include inetd::server

    inetd::service { "time-stream":
        ensure => present,
    }

}

# Set default timezone
#
# === Global variables
#
#   $timezone_set:
#       Time zone as defined under /usr/share/zoneinfo/,
#       for example "Europe/Helsinki". Defaults to "UTC".
#
class time::zone {

    if ! $timezone_set {
        $timezone_set = "UTC"
    }

    file { "/etc/localtime":
        ensure => link,
        target => "/usr/share/zoneinfo/${timezone_set}",
    }

    case $::operatingsystem {
        "centos","redhat": {
            file { "/etc/sysconfig/clock":
                ensure  => present,
                mode    => "0644",
                owner   => "root",
                group   => "root",
                content => template("time/sysconfig_clock.erb"),
            }
        }
        "ubuntu": {
            file { "/etc/timezone":
                ensure  => present,
                mode    => "0644",
                owner   => "root",
                group   => "root",
                content => "${timezone_set}\n",
            }
        }
        "openbsd": { } # file /etc/localtime is enough
        "default": {
            fail("time::zone not supported on ${::operatingsystem}")
        }
    }

}
