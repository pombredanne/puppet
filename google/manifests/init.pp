# Install common prequisites for Google products.
#
class google::common {

    case $::operatingsystem {
        "centos","redhat","fedora": {
            include yum::repo::google
        }
        default: {
            fail("Google products via puppet not supported on ${::operatingsystem}")
        }
    }

}


# Install Google Chrome browser.
#
# == Parameters:
#
#     $managed:
#         Source for managed profile settings, defaults to none.
#
#     $recommended:
#         Source for recommended profile settings, defaults to none.
#
class google::chrome($managed=undef, $recommended=undef) {

    include google::common
    package { "google-chrome-beta":
        ensure  => installed,
        require => Class["google::common"],
    }

    if $managed or $recommended {
        file { [ "/etc/opt", "/etc/opt/chrome", "/etc/opt/chrome/policies", ]:
            ensure => directory,
            mode   => "0755",
            owner  => "root",
            group  => "root",
        }
        if $managed {
            file { "/etc/opt/chrome/policies/managed":
                ensure  => directory,
                mode    => "0755",
                owner   => "root",
                group   => "root",
                require => File["/etc/opt/chrome/policies"],
            }
            file { "/etc/opt/chrome/policies/managed/defaults.json":
                ensure  => present,
                source  => $managed,
                mode    => "0644",
                owner   => "root",
                group   => "root",
                require => File["/etc/opt/chrome/policies/managed"],
            }
        }
        if $recommended {
            file { "/etc/opt/chrome/policies/recommended":
                ensure  => directory,
                mode    => "0755",
                owner   => "root",
                group   => "root",
                require => File["/etc/opt/chrome/policies"],
            }
            file { "/etc/opt/chrome/policies/recommended/defaults.json":
                ensure  => present,
                source  => $recommended,
                mode    => "0644",
                owner   => "root",
                group   => "root",
                require => File["/etc/opt/chrome/policies/recommended"],
            }
        }
    }

}


# Install Google Desktop.
#
class google::desktop {

    include google::common
    package { "google-desktop-linux":
        ensure  => installed,
        require => Class["google::common"],
    }

}
