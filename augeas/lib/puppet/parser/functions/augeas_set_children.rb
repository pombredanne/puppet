module Puppet::Parser::Functions
  newfunction(:augeas_set_children, :type => :rvalue) do |args|
    if args.length != 2
      raise Puppet::ParseError, ('augeas_set_children(): wrong number of arguments (#{args.length}; must be 2)')
    end

    key = args[0]
    values = args[1]

    if values.empty?
      changes = [ "rm %s" % key ]
    else
      changes = [ "clear %s" % key ]
    end

    values.uniq.each.with_index do |value, i|
      changes << "set %s/%s '%s'" % [ key, i+1, value ]
    end

    changes.join("\n")
  end
end
