module Puppet::Parser::Functions
    newfunction(:to_array, :type => :rvalue) do |args|
        if args.length != 1
            raise Puppet::ParseError, ("to_array(): wrong number of arguments (#{args.length}; must be 1)")
        end
        if args[0].is_a?(Array) then
            args[0]
        else
            [ args[0] ]
        end
    end
end

