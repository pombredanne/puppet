
module Puppet::Parser::Functions
    newfunction(:avahi_service_port, :type => :rvalue) do |args|

	type, name, proto =  /^_([^\.]*)\._([^\.]*)$/.match(args[0]).to_a

	port = nil

	File.open('/etc/services', 'r') do |fd|
	    while (line = fd.gets)
		begin
		    port = /^#{name}[ \t]+(\d+)\/#{proto}.*/.match(line)[1]
		    break
		rescue
		    nil
		end
	    end
	end

	if not port
	    raise Puppet::ParseError, 'Failed to get port for service %s' % type
	end
	port

    end
end
