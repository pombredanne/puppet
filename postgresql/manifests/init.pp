
# Install PostgreSQL client
#
class postgresql::client {

    package { "postgresql":
        ensure => installed,
    }

}


# Install PostgreSQL Configuation Tuner
#
class postgresql::pgtune {

    package { "pgtune":
        ensure => installed,
    }

}


# Install PostgreSQL server
#
# === Parameters:
#
#   $datadir:
#       Data directory for databases.
#
class postgresql::server($datadir="/srv/pgsql") {

    case $::operatingsystem {
        "centos","redhat": {
            file { "/etc/sysconfig/pgsql/postgresql":
                ensure  => present,
                content => "PGDATA=/srv/pgsql\n",
                mode    => "0644",
                owner   => "root",
                group   => "root",
                require => Package["postgresql-server"],
                notify  => Service["postgresql"],
            }
        }
        default: {
            fail("postgresql::server not supported in ${::operatingsystem}")
        }
    }

    exec { "service postgresql initdb":
        user    => "root",
        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
        creates => "/srv/pgsql/postgresql.conf",
        require => File["/srv/pgsql"],
        before  => Service["postgresql"],
    }

    if $datadir != "/srv/pgsql" {
        file { $datadir:
            ensure  => directory,
            mode    => "0700",
            owner   => "postgres",
            group   => "postgres",
            seltype => "postgresql_db_t",
            require => Package["postgresql-server"],
        }
        file { "/srv/pgsql":
            ensure  => link,
            target  => $datadir,
            owner   => "root",
            group   => "root",
            seltype => "postgresql_db_t",
            require => File[$datadir],
        }
        selinux::manage_fcontext { "${datadir}(/.*)?":
            type   => "postgresql_db_t",
            before => File[$datadir],
        }
    } else {
        file { "/srv/pgsql":
            ensure  => directory,
            mode    => "0700",
            owner   => "postgres",
            group   => "postgres",
            seltype => "postgresql_db_t",
            require => Package["postgresql-server"],
        }
    }
    selinux::manage_fcontext { "/srv/pgsql(/.*)?":
        type   => "postgresql_db_t",
        before => File["/srv/pgsql"],
    }

    package { "postgresql-server":
        ensure => installed,
    }

    service { "postgresql":
        ensure    => running,
        enable    => true,
        hasstatus => true,
    }

}


# Install PostgreSQL daily backup job
#
# === Global variables
#
#   $datadir:
#       Directory where PostgreSQL backups are stored. Defaults
#       to /srv/pgsql-backup
#
#   $maxage:
#       How long to keep MariaDB backups. Defaults to 7 days.
#
class postgresql::server::backup($datadir="/srv/pgsql-backup", $maxage="7") {

    file { $datadir:
        ensure  => directory,
        mode    => "0700",
        owner   => "postgres",
        group   => "postgres",
    }

    file { "/usr/local/sbin/pgsql-backup":
        ensure  => present,
        content => template("postgresql/pgsql-backup.erb"),
        mode    => "0755",
        owner   => "root",
        group   => "root",
        require => File[$datadir],
    }

    cron { "pgsql-backup":
        command     => "/usr/local/sbin/pgsql-backup",
        environment => [ "MAILTO=root", ],
        user        => "postgres",
        hour        => "0",
        minute      => "20",
        require     => File["/usr/local/sbin/pgsql-backup"],
    }

}
