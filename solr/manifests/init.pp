# Install Apache Solr.
#
# === Parameters
#
# $cores:
#   List of cores to enable.
#
# $datadir:
#   Solr data directory. Defaults to "/srv/solr".
#
# $webhosts:
#   List of Solr virtual hosts.
#
# $htaccess:
#   Source for htaccess file.
#
class solr(
  $cores,
  $datadir='/srv/solr',
  $webhosts=undef,
  $htaccess='puppet:///modules/solr/htaccess',
) {

  if ! ($::apache::sslserver::user and $::apache::sslserver::group) {
    fail('Must declare apache before solr')
  }

  require openjdk::jre

  if ! $solr_package {
    if $::solr_package_latest {
      $solr_package = $::solr_package_latest
    } else {
      fail('Must define $solr_package or $solr_package_latest')
    }
  }

  file { '/usr/local/src/solr.tgz':
    ensure => present,
    mode   => '0644',
    owner  => 'root',
    group  => 'root',
    source => "puppet:///files/packages/${solr_package}"
  }

  util::extract::tar { '/usr/local/share/solr':
    ensure  => latest,
    strip   => '1',
    source  => '/usr/local/src/solr.tgz',
    require => File['/usr/local/src/solr.tgz'],
  }

  include user::system
  realize(User['solr'], Group['solr'])

  if $datadir != '/srv/solr' {
    file { '/srv/solr':
      ensure => link,
      target => $datadir,
    }
  }

  file { $datadir:
    ensure => directory,
    mode   => '0770',
    owner  => 'solr',
    group  => 'solr',
  }

  file { [
    '/srv/solr/cores',
    '/srv/solr/cores/lib',
    '/srv/solr/index',
    '/srv/solr/run',
    '/srv/solr/run/logs',
    '/srv/solr/run/solr-webapp',
    '/srv/solr/spool',
  ]:
    ensure => directory,
    mode   => '2770',
    owner  => 'solr',
    group  => 'solr',
  }

  file { '/srv/solr/cores/solr.xml':
    ensure  => present,
    mode    => '0660',
    owner   => 'solr',
    group   => 'solr',
    content => template('solr/solr.xml.erb'),
    notify  => Service['solr'],
  }

  file { '/srv/solr/run/start.jar':
    ensure => link,
    target => '/usr/local/share/solr/example/start.jar',
    before => Service['solr'],
  }
  file { '/srv/solr/run/contexts':
    ensure => link,
    target => '/usr/local/share/solr/example/contexts',
    before => Service['solr'],
  }
  file { '/srv/solr/run/etc':
    ensure => link,
    target => '/usr/local/share/solr/example/etc',
    before => Service['solr'],
  }
  file { '/srv/solr/run/etc/jetty.xml':
    ensure => present,
    mode   => '0644',
    owner  => 'root',
    group  => 'root',
    source => 'puppet:///modules/solr/jetty.xml',
    notify => Service['solr'],
  }
  file { '/srv/solr/run/lib':
    ensure => link,
    target => '/usr/local/share/solr/example/lib',
    before => Service['solr'],
  }
  file { '/srv/solr/run/resources':
    ensure => link,
    target => '/usr/local/share/solr/example/resources',
    before => Service['solr'],
  }
  file { '/srv/solr/run/webapps':
    ensure => link,
    target => '/usr/local/share/solr/example/webapps',
    before => Service['solr'],
  }

  file { '/etc/init.d/solr':
    ensure  => present,
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    content => template('solr/solr.init.erb'),
    notify  => Exec['add-service-solr'],
  }
  exec { 'add-service-solr':
    path        => '/bin:/usr/bin:/sbin:/usr/sbin',
    command     => $::operatingsystem ? {
      'debian' => 'update-rc.d solr defaults',
      'ubuntu' => 'update-rc.d solr defaults',
      default  => 'chkconfig --add solr',
    },
    refreshonly => true,
    before      => Service['solr'],
  }

  service { 'solr':
    ensure     => running,
    enable     => true,
    hasstatus  => true,
    hasrestart => true,
  }

  file { '/etc/solr':
    ensure => directory,
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
  }
  file { '/etc/solr/htpasswd':
    ensure => present,
    mode   => '0640',
    owner  => 'root',
    group  => $::apache::sslserver::group,
  }

  $htdocs = '/usr/local/share/solr/htdocs'

  file { $htdocs:
    ensure  => directory,
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    require => Util::Extract::Tar['/usr/local/share/solr'],
  }
  file { "${htdocs}/.htaccess":
    ensure => present,
    mode   => '0644',
    owner  => 'root',
    group  => 'root',
    source => $htaccess,
  }

  if $webhosts {
    include apache::mod::proxy
    include apache::mod::proxy_http
    include apache::mod::rewrite

    apache::configfile { 'solr.conf':
      http   => false,
      source => 'puppet:///modules/solr/solr-httpd.conf',
    }

    solr::configwebhost { $webhosts:
      htdocs => $htdocs,
    }
  }

  if ! $solr_requests_package {
    if $::solr_requests_package_latest {
      $solr_requests_package = $::solr_requests_package_latest
    } else {
      fail('Must define $solr_requests_package or $solr_requests_package_latest')
    }
  }

  if ! $solr_pysolr_package {
    if $::solr_pysolr_package_latest {
      $solr_pysolr_package = $::solr_pysolr_package_latest
    } else {
      fail('Must define $solr_pysolr_package or $solr_pysolr_package_latest')
    }
  }

  python::setup::install { '/usr/local/src/requests':
    source => "puppet:///files/packages/${solr_requests_package}",
  }

  python::setup::install { '/usr/local/src/pysolr':
    source  => "puppet:///files/packages/${solr_pysolr_package}",
    require => Python::Setup::Install['/usr/local/src/requests'],
  }

}


# Enable solr for virtual host.
#
define solr::configwebhost($htdocs) {

  file { "/srv/www/https/${name}/solr":
    ensure  => link,
    target  => $htdocs,
    require => File["/srv/www/https/${name}"],
  }

}
