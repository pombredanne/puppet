
# Install DenyHosts
#
# === Parameters
#
#   $whitelist:
#       Source to file containing whitelisted IP addresses. See
#       http://denyhosts.sourceforge.net/faq.html#allowed
#
class denyhosts($whitelist = undef) {

    package { "denyhosts":
        ensure => installed,
    }

    if $whitelist {
        file { "/var/lib/denyhosts/allowed-hosts":
            ensure  => present,
            source  => $whitelist,
            mode    => "0644",
            owner   => "root",
            group   => "root",
            require => Package["denyhosts"],
            notify  => Service["denyhosts"],
        }
    }

    service { "denyhosts":
        ensure  => running,
        enable  => true,
        require => Package["denyhosts"],
    }

}
