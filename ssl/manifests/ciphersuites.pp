#
# Changelog:
#
#   Lets keep changelog for this file for easier auditing. Newest
#   changes on top.
#
#   2014-04-14, oherrala
#     Mozilla's recommended cipersuite list (document version 2.5.1)
#   2014-07-21, osalmi
#     Update to Version 3.1
#   2014-12-19, osalmi
#     Use the Intermediate compatibility configuration (Version 3.4)
#   2015-03-10, osalmi
#     Update ciphersuites to version 4, add also the modern ciphersuites
#
class ssl::ciphersuites {

  #
  # Mozilla (document version 4)
  #
  #   Ref: https://wiki.mozilla.org/Security/Server_Side_TLS
  #
  $mozilla_modern_ciphersuites = 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256'

  $mozilla_intermediate_ciphersuites = 'ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:DES-CBC3-SHA:!DSS'

  #
  # Default ciphersuites
  #
  #   To overwrite default ciphersuites, add $site_ciphersuites with
  #   list of you preferred ones.
  #
  $default_ciphersuites = $mozilla_intermediate_ciphersuites

}
