
# Set system locale
#
# === Parameters
#
#     $lang:
#         Value to set into $LANG environment. Defaults to en_US.
#     $collate:
#         Value to set for $LC_COLLATE environment.
#     $ctype:
#         Value to set for $LC_CTYPE environment.
#     $monetary:
#         Value to set for $LC_MONETARY environment.
#     $numeric:
#         Value to set for $LC_NUMERIC environment.
#     $time:
#         Value to set for $LC_TIME environment.
#     $messages:
#         Value to set for $LC_MESSAGES environment.
#
# === Sample usage
#
# class { "locale":
#     lang  => "en_US.ISO8859-1",
#     ctype => "en_US.UTF-8",
# }
#
class locale(
    $lang="en_US",
    $collate=undef,
    $ctype=undef,
    $monetary=undef,
    $numeric=undef,
    $time=undef,
    $messages=undef
) {

    case $::operatingsystem {
        "centos": {
            if versioncmp($::operatingsystemrelease, 7) < 0 {
                $config = "/etc/sysconfig/i18n"
            } else {
                $config = "/etc/locale.conf"
            }
        }
        "fedora": {
            if versioncmp($::operatingsystemrelease, 18) < 0 {
                $config = "/etc/sysconfig/i18n"
            } else {
                $config = "/etc/locale.conf"
            }
        }
        "debian","ubuntu": {
            $config = "/etc/default/locale"
        }
        default: {
            fail("locale module not supported in ${::operatingsystem}")
        }
    }

    augeas { "locale":
        context => "/files${config}",
        changes => [ "set LANG ${lang}",
                     $collate ? {
                         undef   => "rm LC_COLLATE",
                         default => "set LC_COLLATE ${collate}",
                     },
                     $ctype ? {
                         undef   => "rm LC_CTYPE",
                         default => "set LC_CTYPE ${ctype}",
                     },
                     $monetary ? {
                         undef   => "rm LC_CTYPE",
                         default => "set LC_CTYPE ${monetary}",
                     },
                     $numeric ? {
                         undef   => "rm LC_CTYPE",
                         default => "set LC_CTYPE ${numeric}",
                     },
                     $time ? {
                         undef   => "rm LC_CTYPE",
                         default => "set LC_CTYPE ${time}",
                     },
                     $messages ? {
                         undef   => "rm LC_CTYPE",
                         default => "set LC_CTYPE ${messages}",
                     },
                   ],
    }

}

