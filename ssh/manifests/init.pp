# Export and collect public host keys.
#
class ssh::known_hosts {

    file { "/etc/ssh/ssh_known_hosts":
        ensure => present,
        mode   => "0644",
        owner  => root,
        group  => $::operatingsystem ? {
            OpenBSD => wheel,
            default => root,
        },
    }

    $aliases = merge(inline_template("<%= @homename.split('.')[0] %>"),
                     $::ipaddress,
                     $::ipaddress6,
                     $::ec2_public_ipv4)

    @@sshkey { $::homename:
        ensure       => present,
        type         => rsa,
        key          => $::sshrsakey,
        host_aliases => $aliases,
        require      => File["/etc/ssh/ssh_known_hosts"],
    }

    Sshkey <<| |>>

}


# Install SSH host keys.
#
class ssh::hostkeys {

    tag("bootstrap")

    file { "/etc/ssh/ssh_host_dsa_key":
        ensure => present,
        source => "puppet:///private/ssh_host_dsa_key",
        mode   => "0600",
        owner  => root,
        group  => $::operatingsystem ? {
            openbsd => wheel,
            default => root,
        },
    }
    file { "/etc/ssh/ssh_host_dsa_key.pub":
        ensure => present,
        source => "puppet:///private/ssh_host_dsa_key.pub",
        mode   => "0644",
        owner  => root,
        group  => $::operatingsystem ? {
            openbsd => wheel,
            default => root,
        },
    }

    file { "/etc/ssh/ssh_host_rsa_key":
        ensure => present,
        source => "puppet:///private/ssh_host_rsa_key",
        mode   => "0600",
        owner  => root,
        group  => $::operatingsystem ? {
            openbsd => wheel,
            default => root,
        },
    }
    file { "/etc/ssh/ssh_host_rsa_key.pub":
        ensure => present,
        source => "puppet:///private/ssh_host_rsa_key.pub",
        mode   => "0644",
        owner  => root,
        group  => $::operatingsystem ? {
            openbsd => wheel,
            default => root,
        },
    }

    file { "/etc/ssh/ssh_host_key":
        ensure => present,
        source => "puppet:///private/ssh_host_key",
        mode   => "0600",
        owner  => root,
        group  => $::operatingsystem ? {
            openbsd => wheel,
            default => root,
        },
    }
    file { "/etc/ssh/ssh_host_key.pub":
        ensure => present,
        source => "puppet:///private/ssh_host_key.pub",
        mode   => "0644",
        owner  => root,
        group  => $::operatingsystem ? {
            openbsd => wheel,
            default => root,
        },
    }

}


# Enable SSH server.
#
class ssh::server {

    if $::operatingsystem != "OpenBSD" {
        package { "ssh-server":
            ensure => installed,
            name   => $::operatingsystem ? {
                "openwrt" => "dropbear",
                default   => "openssh-server",
            },
            before => Service["sshd"],
        }

        # Needed for X11 Forwarding
        case $::operatingsystem {
            "centos","redhat","fedora": {
                package { "xorg-x11-xauth":
                    ensure => installed,
                }
            }
            "debian","ubuntu": {
                package { "libxau6":
                    ensure => installed,
                }
            }
            default: {}
        }
    }

    service { "sshd":
        ensure => running,
        enable => true,
        name   => $::operatingsystem ? {
            "debian"  => "ssh",
            "openwrt" => "dropbear",
            "ubuntu"  => "ssh",
            default   => "sshd",
        },
    }

}


# Disable SSH server.
#
class ssh::disable inherits ssh::server {

    case $::operatingsystem {
        "ubuntu": {
            file { "/etc/init/ssh.conf":
                ensure => present,
                mode   => "0644",
                owner  => root,
                group  => root,
                source => "puppet:///modules/ssh/ssh.disabled.conf",
                before => Service["sshd"],
            }
        }
        default: { }
    }

    Service["sshd"] {
        ensure => stopped,
        enable => false,
    }

}


# Set AllowGroups in sshd_config.
#
# === Global variables
#
#   $ssh_allowgroups:
#       Array of groups, root or wheel is always allowed.
#
class ssh::allowgroups {

    warning("ssh::allowgroups is deprecated, use ssh::allow")

    class { "ssh::allow":
        groups => $ssh_allowgroups,
    }

}


# Set AllowUsers and AllowGroups in sshd_config.
#
# === Parameters
#
#   $root:
#     Always allow root. Defaults to true.
#   $users:
#     Array of allowed users. Allow all if undefined.
#   $groups:
#     Array of allowed groups. Allow all if undefined.
#
class ssh::allow(
  $root=true,
  $users=undef,
  $groups=undef,
) {

  include ssh::server

  if $users {
    if $root {
      $users_real = merge('root', $users)
    } else {
      $users_real = $users
    }
  } else {
    $users_real = []
  }

  $root_group = $::operatingsystem ? {
    'openbsd' => 'wheel',
    default   => 'root',
  }

  if $groups {
    if $root {
      $groups_real = merge($root_group, $groups)
    } else {
      $groups_real = $groups
    }
  } else {
    $groups_real = []
  }

  augeas { 'ssh-allow-users':
    context => '/files/etc/ssh/sshd_config',
    changes => augeas_set_children('AllowUsers', $users_real),
    notify  => Service['sshd'],
  }

  augeas { 'ssh-allow-groups':
    context => '/files/etc/ssh/sshd_config',
    changes => augeas_set_children('AllowGroups', $groups_real),
    notify  => Service['sshd'],
  }

}
