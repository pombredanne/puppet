# Install Clarified Analyzer.
#
class clarified::analyzer {

  if ! $clarified_analyzer_package {
    if $::clarified_analyzer_package_latest {
      $clarified_analyzer_package = $::clarified_analyzer_package_latest
    } else {
      fail('Must define $clarified_analyzer_package or $clarified_analyzer_package_latest')
    }
  }

  file { '/usr/local/src/clarified-analyzer-linux.sh':
    ensure => present,
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
    source => "puppet:///files/packages/${clarified_analyzer_package}",
    nofity => Exec['rm -f /usr/local/clarified-analyzer'],
  }
  exec { 'rm -f /usr/local/clarified-analyzer':
    refreshonly => true,
    path        => '/bin:/usr/bin:/sbin:/usr/sbin',
    onlyif      => 'test -h /usr/local/clarified-analyzer',
    before      => Exec['/usr/local/src/clarified-analyzer-linux.sh'],
  }
  exec { '/usr/local/src/clarified-analyzer-linux.sh':
    creates => '/usr/local/clarified-analyzer',
  }

}


# Install Clarified Recorder.
#
# === Parameters
#
# $enable:
#   Install and enable init script. Defaults to true.
#
# $datadir:
#   Recorder data directory. Defaults to /var/lib/recorder.
#
class clarified::recorder(
  $enable=true,
  $datadir=undef,
) {

  if ! $clarified_recorder_package {
    if $::clarified_recorder_package_latest {
      $clarified_recorder_package = $::clarified_recorder_package_latest
    } else {
      fail('Must define $clarified_recorder_package or $clarified_recorder_package_latest')
    }
  }

  if $datadir {
    file { $datadir:
      ensure => directory,
      mode   => '0700',
      owner  => 'root',
      group  => 'root',
    }

    file { '/var/lib/recorder':
      ensure => link,
      target => $datadir,
    }
  } else {
    file { '/var/lib/recorder':
      ensure => directory,
      mode   => '0700',
      owner  => 'root',
      group  => 'root',
    }
  }

  file { [
    '/etc/clarified',
    '/etc/clarified/probe.d',
    '/etc/clarified/remote.d',
  ]:
    ensure => directory,
    mode   => '0644',
    owner  => 'root',
    group  => 'root',
    before => Exec['/usr/local/src/clarified-recorder-linux.sh'],
  }

  File['/etc/clarified/probe.d', '/etc/clarified/remote.d'] {
    purge   => true,
    force   => true,
    recurse => true,
    source  => 'puppet:///modules/custom/empty',
  }

  file { '/usr/local/src/clarified-recorder-linux.sh':
    ensure => present,
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
    source => "puppet:///files/packages/${clarified_recorder_package}",
    notify => Exec['rm -f /usr/local/probe'],
  }
  exec { 'rm -f /usr/local/probe':
    refreshonly => true,
    path        => '/bin:/usr/bin:/sbin:/usr/sbin',
    onlyif      => 'test -h /usr/local/probe',
    before      => Exec['/usr/local/src/clarified-recorder-linux.sh'],
  }
  exec { '/usr/local/src/clarified-recorder-linux.sh':
    creates => '/usr/local/probe',
  }

  exec { 'clarified-functions':
    refreshonly => true,
    path        => '/bin:/usr/bin:/sbin:/usr/sbin',
    cwd         => '/usr/local/probe',
    command     => 'sed s:@PREFIX@:/usr/local/probe: clarified-functions.in > /etc/clarified/clarified-functions',
    subscribe   => Exec['/usr/local/src/clarified-recorder-linux.sh'],
  }

  if $enable == true {
    file { '/etc/init.d/clarified-probe':
      ensure  => present,
      mode    => '0755',
      owner   => 'root',
      group   => 'root',
      source  => '/usr/local/probe/probe-init.sh',
      require => Exec['/usr/local/src/clarified-recorder-linux.sh'],
      notify  => Exec['add-service-clarified-probe'],
    }
    exec { 'add-service-clarified-probe':
      refreshonly => true,
      path        => '/bin:/usr/bin:/sbin:/usr/sbin',
      command     => $::operatingsystem ? {
        'debian' => 'update-rc.d clarified-probe defaults',
        'ubuntu' => 'update-rc.d clarified-probe defaults',
        default  => 'chkconfig --add clarified-probe',
      },
      before      => Service['clarified-probe'],
    }

    service { 'clarified-probe':
      enable => true,
    }
  }

}


# Configure probe.
#
# === Parameters
#
# $name:
#   Probe name.
#
# $interface:
#   Capture interface. Defaults to probe name.
#
# $snaplen:
#   Snaplen. Defaults to 65535.
#
# $keeptime:
#   Amount of data to keep. Defaults to 100GB.
#
# $blocksize:
#   Storage block size. Defaults to 1GB.
#
# $filter:
#   Optional filter expression.
#
# $remoteport:
#   Remote port. Defaults to 10000.
#
# $collab:
#   List of collabs for authentication.
#
# $probe:
#   Enable probe. Defaults to true.
#
# $remote:
#   Enable remote. Defaults to true.
#
# === Sample usage
#
# clarified::probe { 'eth0':
#   keeptime  => '500GB',
#   blocksize => '10GB',
#   filter    => 'host 192.168.1.1',
#   collab    => [ 'collabname:PageName' ],
# }
#
define clarified::probe(
  $interface=undef,
  $snaplen='65535',
  $keeptime='100GB',
  $blocksize='1GB',
  $filter='',
  $remoteport='10000',
  $collab=[],
  $probeopt='',
  $remoteopt='',
  $probe=true,
  $remote=true
) {

  Class['clarified::recorder'] -> Clarified::Probe[$name]

  if $interface {
    $interface_real = $interface
  } else {
    $interface_real = $name
  }

  file { "/var/lib/recorder/${name}":
    ensure => directory,
    mode   => '0700',
    owner  => 'root',
    group  => 'root',
  }

  file { "/etc/clarified/probe.d/${name}":
    ensure  => $probe ? {
      true  => present,
      false => absent,
    },
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    content => template('clarified/probe.erb'),
    require => File["/var/lib/recorder/${name}"],
    notify  => $probe ? {
      true  => Service["probe-${name}"],
      false => undef,
    },
  }

  service { "probe-${name}":
    ensure    => $probe ? {
      true  => running,
      false => stopped,
    },
    provider  => 'base',
    start     => "/etc/clarified/probe.d/${name} start",
    restart   => "/etc/clarified/probe.d/${name} restart",
    stop      => "pkill -f /var/run/probe/${name}.pid",
    status    => "pgrep -f /var/run/probe/${name}.pid",
    subscribe => Exec['/usr/local/src/clarified-recorder-linux.sh'],
  }

  file { "/etc/clarified/remote.d/${name}":
    ensure  => $remote ? {
      true  => present,
      false => absent,
    },
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    content => template('clarified/remote.erb'),
    require => File["/var/lib/recorder/${name}"],
    notify  => $remote ? {
      true  => Service["remote-${name}"],
      false => undef,
    },
  }

  service { "remote-${name}":
    ensure    => $remote ? {
      true  => running,
      false => stopped,
    },
    provider  => 'base',
    start     => "/etc/clarified/remote.d/${name} start",
    restart   => "/etc/clarified/remote.d/${name} restart",
    stop      => "pkill -f /var/run/remote/${name}.pid",
    status    => "pgrep -f /var/run/remote/${name}.pid",
    require   => Service["probe-${name}"],
    subscribe => Exec['/usr/local/src/clarified-recorder-linux.sh'],
  }

}
