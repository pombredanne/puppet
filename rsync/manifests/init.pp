
# Install rsync
#
class rsync {

    package { "rsync":
        ensure => present,
    }

}

class rsync::run-rsync {
    case $::operatingsystem {
        ubuntu: {
            package { "run-rsync": ensure => installed, }
            file { "/etc/run-rsync/RunRsyncConfig.pm":
                ensure  => present,
                source  => [ "puppet:///files/run-rsync/${homename}-RunRsyncConfig.pm",
                             "/etc/run-rsync/RunRsyncConfig.pm", ],
                mode    => "0644",
                owner   => "root",
                group   => "root",
            }
        }
    }
}


# Set up environment to execute backup jobs
#
#   $rsync_logdir:
#     (optional) Rsync job logs goes to this directory. Defaults to "/srv/rsync-log"
#
class rsync::backup {

  package { "util-linux-ng": ensure => installed, } # Needed for /usr/bin/flock
  
  if $rsync_logdir {
    file { $rsync_logdir:
      ensure  => directory,
      owner   => "root",
      group   => "root",
      mode    => 0700,
    }
    file { "/srv/rsync-log":
      ensure  => link,
      target  => $rsync_logdir,
      require => File[$rsync_logdir],
    }
  } else {
    file { "/srv/rsync-log":
      ensure  => directory,
      owner   => "root",
      group   => "root",
      mode    => 0700,
    }
  }

  file { "/usr/local/cron":
    ensure => directory,
    owner  => "root",
    group  => "root",
    mode   => 0755,
  }

  file { "/usr/local/cron/rsync-backup-summary":
    ensure  => present,
    owner   => "root",
    group   => "root",
    mode    => 0755,
    source  => "puppet:///modules/rsync/rsync-backup-summary.sh",
    require => File["/usr/local/cron"],
  }

  cron { "rsync-backup-summary":
    command => "/usr/local/cron/rsync-backup-summary",
    user    => "root",
    hour    => 06,
    minute  => 00,
    require => File["/usr/local/cron/rsync-backup-summary"],
  }

}


# Define rsync backup job
#
#   $name:
#     Cron job's name
#
#   $source_host:
#     (optional) Hostname for source host to pull data from. If
#     defined $target_host can't be defined.
#
#   $target_host:
#     (optional) Hostname for target host to push data to. If defined
#     $source_host can't be defined.
#
#   $source_dirs:
#     Single "/path" or [ "/array", "/of", "/paths" ] of sources to
#     copy.
#
#   $target_dir:
#     Single "/path" for rsync destination
#
#   $hour:
#     The hour at which to run the cron job. See Puppet's "cron" type.
#
#   $minute:
#     (optional) The minute at which to run the cron job.
#
#   $excludes:
#     (optional) String or Array of PATTERNS to exclude. See rsync
#     manual for exclude PATTERNS.
#
#   $enable_acl:
#     (optional) Enable rsync ACL. Defaults to enabled. Set to false
#     to disable.
#
#   $options:
#     (optional) Rsync options for job. For example "--bwlimit=300".
#
define rsync::backup_job (
  $source_host="",
  $target_host="",
  $source_dirs,
  $target_dir,
  $excludes="",
  $hour,
  $minute=0,
  $enable_acl=true,
  $options="",
  ) {

    include rsync
    include rsync::backup

    $_script = "/usr/local/cron/${name}"

    if $source_host and $target_host {
      fail("Both source_host and target_host can't be defined together.")
    }

    if $source_host and $source_dirs {
      $_source_dirs = is_array($source_dirs) ? {
        # See rsync man page's advanced usage section for this syntax
        true  => inline_template("<%= @source_dirs.join(\" :\") -%>"),
        false => $source_dirs,
      }
      $_source = "${source_host}:${_source_dirs}"
      $_log_host = $source_host
    } elsif $source_dirs {
      $_source = is_array($source_dirs) ? {
        true  => inline_template("<%= @source_dirs.join(\" \") -%>"),
        false => $source_dirs,
      }
    } else {
      fail("You need to specify source_dirs (and optionally source_host)")
    }

    if $target_host and $target_dir {
      $_target = "${target_host}:${target_dir}"
      $_log_host = $target_host
    } elsif $target_dir {
      $_target = "${target_dir}"
    } else {
      fail("You need to specify target_dir (and optionally target_host)")
    }

    if !$_log_host {
      $_log_host = "localhost"
    }

    if is_array($excludes) {
      $_excludes = inline_template('<%= @excludes.map {|x| "--exclude \'" + x + "\'"}.join(" ") -%>')
    } elsif $excludes {
      # We assume string here
      $_excludes = "--exclude='$excludes'"
    } else {
      $_excludes = ""
    }

    if ! $target_host and $target_dir {
      file { $target_dir:
        ensure => directory,
        owner  => "root",
        group  => "root",
        mode   => 0700,
        before => File["/usr/local/cron/${name}"],
      }
    }

    file { $_script:
      ensure  => file,
      content => template("rsync/rsync-backup.sh.erb"),
      owner   => "root",
      group   => "root",
      mode    => 0744,
      require => File["/srv/rsync-log"],
    }

    cron { "rsync-backup-${name}":
      command => $_script,
      user    => "root",
      hour    => $hour,
      minute  => $minute,
      require => File[$_script],
    }

}
