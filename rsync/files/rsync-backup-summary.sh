#!/bin/sh

RSYNC_LOGDIR="/srv/rsync-log"

cd $RSYNC_LOGDIR || exit 1
for host in `ls *.log | cut -d"-" -f1 | sort -u`; do
    LOG=`ls $RSYNC_LOGDIR/$host-*.log | sort -nr | head -n1`
    ls -l --time-style=full-iso $LOG
    grep "^\*\*\*\*\*" $LOG
    echo ""
done
