# Load external key-value facts for facter < 1.7.
if Facter.value(:facterversion).match(/^1\.[56]/) and
   File.directory?("/etc/facter/facts.d")
    Dir.glob("/etc/facter/facts.d/*.txt") do |txt|
        File.readlines(txt).each do |line|
            kv = line.strip.split("=", 2)
            continue if kv.length != 2
            Facter.add(kv[0].to_sym) do
                setcode do
                    kv[1]
                end
            end
        end
    end
end
