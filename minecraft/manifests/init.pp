
# Install Minecraft
#
class minecraft {

    file { "/usr/local/lib/minecraft.jar":
        ensure => present,
        source => "puppet:///files/packages/minecraft.jar",
        mode   => "0644",
        owner  => "root",
        group  => "root",
    }

    file { "/usr/local/bin/minecraft":
        ensure  => present,
        source  => "puppet:///modules/minecraft/minecraft",
        mode    => "0755",
        owner   => "root",
        group   => "root",
        require => File["/usr/local/lib/minecraft.jar"],
    }

}
