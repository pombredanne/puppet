# Install and configure Hastymail.
#
# === Parameters
#
#   $datadir:
#       Data directory. Defaults to /srv/hastymail.
#
class hastymail($datadir=undef) {

    include php::cli
    include php::imap
    include php::ldap
    include php::mcrypt

    if !$hastymail_package {
        if $::hastymail_package_latest {
            $hastymail_package = $::hastymail_package_latest
        } else {
            fail("Must define \$hastymail_package or \$hastymail_package_latest")
        }
    }

    if $datadir {
        file { $datadir:
            ensure  => directory,
            mode    => "0750",
            owner   => "root",
            group   => $apache::sslserver::group,
            seltype => "httpd_sys_rw_content_t",
        }
        selinux::manage_fcontext { "${datadir}(/.*)?":
            type   => "httpd_sys_rw_content_t",
            before => File[$datadir],
        }
        file { "/srv/hastymail":
            ensure  => link,
            target  => $datadir,
            seltype => "httpd_sys_rw_content_t",
            require => File[$datadir],
        }
    } else {
        file { "/srv/hastymail":
            ensure  => directory,
            mode    => "0750",
            owner   => "root",
            group   => $apache::sslserver::group,
            seltype => "httpd_sys_rw_content_t",
        }
    }
    selinux::manage_fcontext { "/srv/hastymail(/.*)?":
        type   => "httpd_sys_rw_content_t",
        before => File["/srv/hastymail"],
    }

    file { [ "/srv/hastymail/attachments",
             "/srv/hastymail/settings", ]:
        ensure  => directory,
        mode    => "0770",
        owner   => "root",
        group   => $apache::sslserver::group,
        seltype => "httpd_sys_rw_content_t",
    }

    file { "/usr/local/src/hastymail.tar.gz":
        ensure => present,
        source => "puppet:///files/packages/${hastymail_package}",
        mode   => "0644",
        owner  => "root",
        group  => "root",
    }
    util::extract::tar { "/usr/local/share/hastymail":
        ensure  => latest,
        strip   => 1,
        source  => "/usr/local/src/hastymail.tar.gz",
        require => File["/usr/local/src/hastymail.tar.gz"],
    }

    file { "/etc/hastymail2":
        ensure => directory,
        mode   => "0750",
        owner  => "root",
        group  => $apache::sslserver::group,
    }
    file { "/etc/hastymail2/hastymail2.conf":
        ensure  => present,
        source  => [ "puppet:///private/hastymail2.conf",
                     "puppet:///files/mail/hastymail2.conf", ],
        mode    => "0640",
        owner   => "root",
        group   => $apache::sslserver::group,
        require => File["/etc/hastymail2"],
    }
    exec { "rm -f /etc/hastymail2/hastymail2.rc && php /usr/local/share/hastymail/install_scripts/install_config.php /etc/hastymail2/hastymail2.conf /etc/hastymail2/hastymail2.rc":
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        refreshonly => true,
        require     => Util::Extract::Tar["/usr/local/share/hastymail"],
        subscribe   => File["/etc/hastymail2/hastymail2.conf"],
        before      => File["/etc/hastymail2/hastymail2.rc"],
    }
    file { "/etc/hastymail2/hastymail2.rc":
        ensure => present,
        mode   => "0640",
        owner  => "root",
        group  => $apache::sslserver::group,
    }

}
