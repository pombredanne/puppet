Facter.add('pythonsitedir') do
    setcode do
        if File.exists?('/usr/local/bin/python')
            pythonbin='/usr/local/bin/python'
        else
            system('which python > /dev/null 2>&1')
            if $? == 0
                pythonbin='python'
            end
        end
        if pythonbin
            case Facter.value(:operatingsystem)
            when "Ubuntu"
                %x{#{pythonbin} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(prefix='/usr/local')"}.chomp
            else
                %x{#{pythonbin} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()"}.chomp
            end
        end
    end
end

Facter.add('pythonversion') do
    setcode do
        if File.exists?('/usr/local/bin/python')
            pythonbin='/usr/local/bin/python'
        else
            system('which python > /dev/null 2>&1')
            if $? == 0
                pythonbin='python'
            end
        end
        if pythonbin
            %x{#{pythonbin} -c "import platform; print platform.python_version()"}.chomp
        end
    end
end
