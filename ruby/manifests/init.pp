# Install rubygems.
#
class ruby::rubygems {

    tag("bootstrap")

    require gnu::gcc
    require gnu::make

    package { "ruby-devel":
        ensure => installed,
        name   => $::operatingsystem ? {
            "ubuntu" => regsubst($::rubyversion, '^([0-9]+\.[0-9]+)\..*', 'ruby\1-dev'),
            default  => "ruby-devel",
        },
    }

    package { "rubygems":
        ensure  => installed,
        require => Package["ruby-devel"],
    }

}


# Install activerecord.
#
class ruby::activerecord {

    case $::operatingsystem {
        "centos","redhat": {
            require yum::repo::puppetlabs::dependencies
            $package = "rubygem-activerecord"
        }
        "debian": {
            $package = regsubst($::rubyversion, '^([0-9]+\.[0-9]+)\..*', 'libactiverecord-ruby\1')
        }
        "ubuntu": {
            if versioncmp($::operatingsystemrelease, "12.04") >= 0 {
                $package = regsubst($::rubyversion, '^([0-9]+\.[0-9]+)\..*', 'libactiverecord-ruby\1')
            } else {
                $package = undef
            }
        }
        default: {
            $package = "rubygem-activerecord"
        }
    }

    if $package {
        package { "rubygem-activerecord":
            ensure => installed,
            name   => $package,
        }
    }

}


# Install mysql ruby bindings.
#
class ruby::mysql {

    package { "ruby-mysql":
        ensure => installed,
        name   => $::operatingsystem ? {
            "debian" => regsubst($::rubyversion, '^([0-9]+\.[0-9]+)\..*', 'libmysql-ruby\1'),
            "ubuntu" => regsubst($::rubyversion, '^([0-9]+\.[0-9]+)\..*', 'libmysql-ruby\1'),
            default  => "ruby-mysql",
        },
    }

}


# Install rails.
#
class ruby::rails {

    case $::operatingsystem {
        "centos","redhat": {
            if $::operatingsystemrelease =~ /^[1-5]/ {
                package { "rubygem-rails":
                    ensure => installed,
                }
            } else {
                require ruby::rubygems
                package { "rubygem-rails":
                    ensure   => "2.3.17",
                    name     => "rails",
                    provider => "gem",
                }
            }
        }
        default: {
            package { "rubygem-rails":
                ensure => installed,
                name   => $::operatingsystem ? {
                    "debian"  => "rails",
                    "ubuntu"  => "rails",
                    "openbsd" => "ruby-rails",
                    default   => "rubygem-rails",
                },
            }
        }
    }

}


# Install rrd ruby bindings.
#
class ruby::rrd {

    case $::operatingsystem {
        "centos","redhat": {
            $package = $::operatingsystemrelease ? {
                /^[1-5]/ => "ruby-RRDtool",
                default  => "rrdtool-ruby",
            }
        }
        "debian","ubuntu": {
            $package = regsubst($::rubyversion, '^([0-9]+\.[0-9]+)\..*', 'librrd-ruby\1')
        }
        "openbsd": {
            $package = "ruby-rrd"
        }
        default: {
            $packaage = "ruby-RRDtool"
        }
    }

    package { "ruby-rrd":
        ensure => installed,
        name   => $package,
    }

}


# Install sqlite3 ruby bindings.
#
class ruby::sqlite3 {

    package { "rubygem-sqlite3-ruby":
        ensure => installed,
        name   => $::operatingsystem ? {
            "debian"  => regsubst($::rubyversion, '^([0-9]+\.[0-9]+)\..*', 'libsqlite3-ruby\1'),
            "ubuntu"  => regsubst($::rubyversion, '^([0-9]+\.[0-9]+)\..*', 'libsqlite3-ruby\1'),
            "openbsd" => "ruby-sqlite3",
            default   => "rubygem-sqlite3-ruby",
        },
    }

}
